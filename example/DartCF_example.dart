import 'package:DartCF/DartCF.dart';

main() {
  print("Método Main");
  //Tipos de datos : number(int-double) , String(cadenas), Booleans,Lists,Maps, Runes,Symbols

 /* var idioma = "ingles";
  String nombre;

  String moneda;
  print(moneda);

  //Numeros
  var numero = 1;
  num x = 1;
  int n = 1;
  n = 10.55;
  print(numero + x + n);
*/

/* int num1 = 1;
 int num2 = 2;

 print(num1 + num2);
 print(num1 <= num2);*/

/* String a = "adrian";
 String m = "mateo";
 String v = "valentina";
 int x = 10;


 print("Hola $a ${m.toUpperCase()} $v ${x}");*/

 //Operadores == != < < <= >=
  var numero = 1;
  var n = 10;
  
  if(numero < n){
    print("Imprime numero $numero");
  }else {
    print("numero>n");
  }

  //as, is, !is
  print(numero is num);

  print("Ciclo for");

  for(var i=0; i<10; i++){
    print("Hola $i");
  }

  // se necesita una variable
  print("while");
  var k = 0;
  while(k<10){
    print("valor de $k");
        k++;
  }

  print("do while");
  var j = 0;
  do{
    print("valor de $j");
    j++;
  }while(j<10);


  print("Switch Case");
  var x = 1;
  switch(x){
    case 1:
      print("1");
      break;
    case 2:
      print("2");
      break;
    case 3:
      print("3");
      break;
    case 4:
      print("4");
      break;
  }



}
